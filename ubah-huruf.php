<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
<?php
function ubah_huruf($string){
//kode di sini
$abjad="abcdefghijklmnopqrstuvwxyz";
$output= "";
for ($i=0; $i < strlen($string); $i++) {
     $position = strpos($abjad, $string[$i]);
     $output .= substr($abjad, $position + 1, 1);
     //$output = output + substr($abjad, $position + 1, 1); diringkas menjadi $output .= substr($abjad, $position + 1, 1);
     }
return $output . "<br>";
}
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu


// TEST CASES
?>
</body>
</html>